create database estrouge;
use estrouge;
/*status will be (0, 1, 2) as (Planning, Doing, Complete)*/
create table work (id INT(6) UNSIGNED auto_increment primary key, work_name VARCHAR(255) NOT NULL,starting_date date not null,ending_date date not null,status tinyint not null); 
INSERT INTO `estrouge`.`work` (`work_name`, `starting_date`, `ending_date`, `status`) VALUES ('work 1', '2019-10-01', '2019-10-02', '0'),
('work 2', '2019-10-02', '2019-10-03', '1'),
('work 3', '2019-10-03', '2019-10-04', '2'),
('work 4', '2019-10-04', '2019-10-05', '0'),
('work 5', '2019-10-05', '2019-10-06', '1'),
('work 6', '2019-10-06', '2019-10-07', '2'),
('work 7', '2019-10-07', '2019-10-08', '0'),
('work 8', '2019-10-08', '2019-10-09', '1'),
('work 9', '2019-10-09', '2019-10-10', '2'),
('work 10', '2019-10-10', '2019-10-11', '0'),
('work 11', '2019-10-11', '2019-10-12', '1'),
('work 12', '2019-10-12', '2019-10-13', '2'),
('work 13', '2019-10-13', '2019-10-14', '0'),
('work 14', '2019-10-14', '2019-10-15', '1'),
('work 15', '2019-10-15', '2019-10-16', '2'),
('work 16', '2019-10-16', '2019-10-17', '0'),
('work 17', '2019-10-17', '2019-10-18', '1');