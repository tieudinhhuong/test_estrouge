package com.estrouge.main.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.estrouge.main.exception.RecordNotFoundException;
import com.estrouge.main.entity.Work;
import com.estrouge.main.service.WorkService;


@Controller
@RequestMapping(path="/work")
public class WorkCotroller {
	@Autowired 
	private WorkService workService;

	@PostMapping(path="/")
	public ResponseEntity<Work> addWork(@RequestBody Work work) {
		Work savedWork = workService.save(work);
		return ResponseEntity.ok(savedWork);
	}
	@PutMapping(path="/{id}")
	public ResponseEntity<Work> editWork(@PathVariable Integer id, @RequestBody Work work){
		Optional<Work> workOptional = workService.findById(id);
		if(!workOptional.isPresent()) {
			throw new RecordNotFoundException(Work.class.getSimpleName(), id);
		}
		work.setId(id);
		workService.save(work);
		return ResponseEntity.ok(work);
	}
	@DeleteMapping(path="/{id}")
	public ResponseEntity<?> deleteWork(@PathVariable Integer id) {
		Optional<Work> workOptional = workService.findById(id);
		if(!workOptional.isPresent()) {
			throw new RecordNotFoundException(Work.class.getSimpleName(), id);
		}
		workService.delete(workOptional.get());
		return ResponseEntity.ok().build();
	}
	@GetMapping(path="/list")
	public @ResponseBody Iterable<Work> getWorks(@RequestParam String sortBy,@RequestParam int page, @RequestParam int pageSize) {
		PageRequest pageRequest = PageRequest.of(page,pageSize,Sort.by(sortBy));
		Page<Work> resultSet = workService.findAll(pageRequest);
		return resultSet.getContent();
	}
}
