package com.estrouge.main.advice;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.estrouge.main.exception.ErrorDetail;
import com.estrouge.main.exception.RecordNotFoundException;

@ControllerAdvice
public class RecordNotFoundAdvice {
	
	@ResponseBody
	@ExceptionHandler(RecordNotFoundException.class)
	ResponseEntity<ErrorDetail> entityNotFoundHandler(RecordNotFoundException ex,HttpServletRequest request) {
		ErrorDetail errorDetail = new ErrorDetail(new Date(), ex.getMessage(),
		        request.getRequestURI(),request.getMethod());
	    return new ResponseEntity<ErrorDetail>(errorDetail, HttpStatus.NOT_FOUND);
	}
}
