package com.estrouge.main.exception;

public class RecordNotFoundException extends RuntimeException{
	public RecordNotFoundException(String entityName, Integer id) {
		super("Could not find " + entityName + " with id " + id);
	}
}
