package com.estrouge.main.exception;

import java.util.Date;

public class ErrorDetail {
    private Date timestamp;
    private String message;
    private String path;
    private String action;
	public ErrorDetail(Date timestamp, String message, String path, String action) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.path = path;
		this.action = action;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
    

}
