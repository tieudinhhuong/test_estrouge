package com.estrouge.main.repository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.estrouge.main.entity.Work;

public interface WorkRepository extends  PagingAndSortingRepository<Work, Integer>{

}
