package com.estrouge.main.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.estrouge.main.entity.Work;
import com.estrouge.main.repository.WorkRepository;



@Service
public class WorkService {
	@Autowired 
	private WorkRepository workRepository;

    public Page<Work> findAll(PageRequest pageRequest) {
        return workRepository.findAll(pageRequest);
    }

    public Optional<Work> findById(Integer id) {
        return workRepository.findById(id);
    }

    public Work save(Work work) {
        return workRepository.save(work);
    }

    public void delete(Work work) {
    	workRepository.delete(work);
    }
    
    public Iterable<Work> findAll() {
    	return workRepository.findAll();
    }
}
