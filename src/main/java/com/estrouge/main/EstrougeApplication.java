package com.estrouge.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EstrougeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EstrougeApplication.class, args);
	}

}
