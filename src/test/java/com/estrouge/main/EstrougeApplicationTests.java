package com.estrouge.main;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.estrouge.main.entity.Work;
import com.estrouge.main.service.WorkService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EstrougeApplication.class)
public class EstrougeApplicationTests {
	protected MockMvc mvc;
	@Autowired
	WebApplicationContext webApplicationContext;

	@MockBean
	WorkService workService;

	@Before
	public void setUp() {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	protected String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
		return objectMapper.writeValueAsString(obj);
	}

	protected <T> T mapFromJson(String json, Class<T> clazz)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, clazz);
	}

	@Test
	public void addWork() throws Exception {
		String uri = "/work/";
		String json = "{\"id\":1,\"workName\":\"Test\",\"startingDate\":\"2019-03-03\",\"endingDate\":\"2019-03-04\",\"status\":0}";
		Work work = this.mapFromJson(json, Work.class);

		given(workService.save(work)).willReturn(work);

		MvcResult mvcResult = mvc
				.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_UTF8).content(json))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Work returnWork = this.mapFromJson(content, Work.class);
		assertEquals(work, returnWork);
	}

	@Test
	public void editWork() throws Exception {
		String uri = "/work/1";
		String json = "{\"id\":1,\"workName\":\"Test\",\"startingDate\":\"2019-03-03\",\"endingDate\":\"2019-03-04\",\"status\":0}";
		Work work = this.mapFromJson(json, Work.class);

		given(workService.findById(1)).willReturn(Optional.of(work));

		MvcResult mvcResult = mvc
				.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_UTF8).content(json))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Work returnWork = this.mapFromJson(content, Work.class);
		assertEquals(work, returnWork);
	}

	@Test
	public void deleteWork() throws Exception {
		String uri = "/work/1";
		String json = "{\"id\":1,\"workName\":\"Test\",\"startingDate\":\"2019-03-03\",\"endingDate\":\"2019-03-04\",\"status\":0}";
		Work work = this.mapFromJson(json, Work.class);

		given(workService.findById(1)).willReturn(Optional.of(work));

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	@Test
	public void listWork() throws Exception {
		String uri = "/work/list";
		List<Work> works = new ArrayList<Work>();
		for (int i = 1; i <= 3; i++) {
			Work work = new Work();
			work.setId(i);
			work.setWorkName("Test " + i);
			work.setStartingDate(Date.valueOf("2019-03-03"));
			work.setEndingDate(Date.valueOf("2019-03-03"));
			work.setStatus(0);
			works.add(work);
		}
		PageRequest pageRequest = PageRequest.of(0, 3, Sort.by("workName"));
		Page<Work> page = new PageImpl<>(works);
		given(workService.findAll(pageRequest)).willReturn(page);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.get(uri).param("page", "0").param("pageSize", "3").param("sortBy", "workName"))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		assertEquals(content, this.mapToJson(works));
	}

}
